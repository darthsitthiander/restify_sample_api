module.exports = {
    ENV: process.env.NODE_ENV || 'development',
    PORT: process.env.PORT || 3000,
    URL: process.env.BASE_URL || 'http://localhost:3000',
    MONGODB_URI: process.env.MONGODB_URI || 'mongodb://admin:xyWDkcFmGQ8GmtA@ds145093.mlab.com:45093/customer_api',
    JWT_SECRET: process.env.JWT_SECRET || 'secret1'
}